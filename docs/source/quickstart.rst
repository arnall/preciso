.. Getting started with Preciso

Getting started
=================================

Overview
--------

Preciso is a Python wrapper to the initial C++ project. It is designed to be **lightweight** and **convenient**, for both new and more experienced users.

Below is an example use case for PreciSo :

.. code-block:: python

    import preciso
    import matplotlib.pyplot as plt

    # a configuration file specific to your material
    config_filename = "path/to/configuration_file.txt"
    
    # communication with PreciSo's C++ engine is transparent :
    results = preciso.runSimulation(config_filename) 

    # results are available in the form of pandas DataFrames
    df = results.precipitation[0] 
    
    plt.loglog(df["t[s]"],
               df["rmean_beta'[m]"],
               label="$R_{mean} (m)$")
    # *A beautiful figure appears*

With its **concise** **high-level** syntax, the Python wrapper allows to **focus on physics** (the parameters, the configuration file) instead of coding. 


Try it online
-------------

An interactive demo of PreciSo is hosted at `myBinder <https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Farnall%2Fpreciso/c3de3e22980aea1e2ed6209d284e9b1b915fbebe?filepath=notebooks%2F01-Overview.ipynb>`_ in the form of a Jupyter Notebook. It allows trying preciso with no installation, from any device.

Installation
------------

PreciSo is available as a *Python package*. To use it, you will need **Python 3**, and a few other packages PreciSo is built on.

You have different options, mostly depending on your operating system.

.. note::
    If you have any issue installing PreciSo, or if this documentation is somehow inaccurate, feel free to submit a ticket to the project's `Gitlab issue tracker <https://framagit.org/arnall/preciso/issues>`_.

.. warning::
    **Installing Python** the first time can be **tedious**, especially on Windows. If you only need very occasional use of PreciSo, you may want to consider using a cloud-based solution (e.g `<https://repl.it>`_).
    
.. warning::    
     A messy Python installation will make using and updating PreciSo a lot harder. To avoid that, **always prefer installing in a virtual environment**, e.g. using **Miniconda** (or pipenv, or virtualenv).

.. note::
    PreciSo supports Python 3 only. Make sure you install a Python version 3.x (3.7 is recommended), not a 2.x.

On Linux/MacOS, using Miniconda
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Miniconda is a distribution that includes `conda`, the package manager that will help you make a clean install of Python and PreciSo on any platform, with no root permissions. The famous Anaconda distribution is similar to Miniconda, but it includes a huge number of additional packages you don't need for using PreciSo.


1. To get Python and Miniconda, follow the **instructions** from the official documentation of `Miniconda <https://docs.conda.io/en/latest/miniconda.html>`_ **and** `conda <https://conda.io/projects/conda/en/latest/user-guide/install/index.html>`_. 

On MacOS and Linux, it should be :

.. code-block:: bash

    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh -O ~/miniconda.sh
    bash ~/miniconda.sh -b -p $HOME/miniconda


2. Once conda is installed, create an environment that lists all Preciso's dependencies. For that, we'll use a file to specify the packages we want to install, that contains :

.. code-block:: python

    name: python-preciso
    channels:
      - anaconda
    dependencies:
      - python=3.7
      - numpy
      - pandas
      - matplotlib
      - scipy
      - jinja2
      - git
      - jupyter
      - seaborn

This file can be **downloaded** `here <https://framagit.org/arnall/preciso/raw/master/environment.yml?inline=false>`_. 

3. Then, run one line at a time :

.. code-block:: python

    # the first time you use conda
    
    # On macOS / Linux :
    $HOME/miniconda/bin/conda init

    # If conda is not found, you'll have to Google for a solution for your specific case/platform, or ask for help

    # create an environment from the file you downloaded
    conda env create -f environment.yml

    # activate the environment everytime you want to use it. 
    # On Linux/MacOS can set an alias in your ~.bashrc or ~.bash_profile file for this one.
    conda activate python-preciso

    # Download the last version of Preciso
    git clone https://framagit.org/arnall/preciso.git

    # Install preciso in developper mode (-e)
    cd preciso
    pip install -e .


4. Check that the installation was successful. The following command should issue no error when ran in a **new** terminal.

.. code-block:: python
    
    python3 -c 'import preciso; print("Preciso was successfully installed !")'



Manual installation on Linux/Mac OS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you already have a full Python installed and ready-to-go, and have git, you can follow these instructions.

Downlaod PreciSo package::

    git clone https://framagit.org/arnall/preciso.git preciso

Upgrade pip::

    python3 -m pip install --user --upgrade pip

In Jupyter, open a new terminal and enter the preciso folder::

    cd preciso

Install preciso package::

    pip install -e .

Check all requirements::

    pip install -r requirements.txt


On Windows (using Anaconda)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To install Python requirements :

1. Install a full Anaconda distribution, following its official documentation. 

.. note::
    
    Alternatively, you can install miniconda by downloading the installer `here <https://conda.io/miniconda.html>`_. Then from a terminal, do :

    .. code-block:: bash

        start /wait "" Miniconda3-latest-Windows-x86_64.exe /InstallationType=JustMe /RegisterPython=0 /S /D=%UserProfile%\miniconda


2. Download the `environment file <https://framagit.org/arnall/preciso/raw/master/environment.yml?inline=false>`_.

3. From Anaconda, `import an environment <https://docs.anaconda.com/anaconda/navigator/tutorials/manage-environments/#importing-an-environment>`_ and **activate it**.

**For the installation of Preciso :**

4. Open an Anaconda prompt, Jupyter terminal or jupyter notebook. Make sure you access your python-preciso environment by typing ``conda env list``. It should display something like :

.. code-block:: bash
    
    # conda environments:
    #
    base                      /home/aallera/miniconda
    python-preciso         *  /home/aallera/miniconda/envs/python-preciso

Check the position of the ``*`` character, which means that ``python-preciso`` is the activated environment.


5. Create a folder and extract the archive found `here <https://framagit.org/arnall/preciso/-/archive/master/preciso-master.zip>`_ (you'll have to manually fetch new versions to get updates and bug fixes !)

or

5. Get preciso source code with ::

    git clone https://framagit.org/arnall/preciso.git
    
It will create a "preciso" folder in the current directory, that will contain the last version of the code. 
Updates can be done from this "preciso" directory with ``git pull``.

6. Go to the folder that contains preciso's `setup.py`::

    cd preciso

Then install preciso with pip (if error, try `pip3` instead)::

    pip install -e .

If executed from a Jupyter notebook, do instead ::

    !pip install -e .

Relaunch Jupiter Notebook and check that preciso is importable from anywhere::

    import preciso; preciso.runSimulation("", temp=False)
