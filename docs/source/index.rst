.. Preciso documentation master file, created by
   sphinx-quickstart on Mon Apr  8 11:12:51 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. include:: ../../README.rst
   :start-after: placeholder-for-doc-index


.. toctree::
   :maxdepth: 2
   :caption: Documentation contents:

   quickstart
   tutorial
   preciso
   


.. Indices and tables
.. ==================

.. * :ref:`modindex`

.. * :ref:`genindex`
.. * :ref:`search`
