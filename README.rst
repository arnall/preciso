IMPORTANT  
=========

2/07/2020 : Due to the scheduled closure of framagit, this project has been moved to gitlab.com : https://gitlab.com/Arnall/preciso 

It will **not be updated anymore** and will eventually become inaccessible.


.. image:: https://img.shields.io/badge/Documentation-PreciSo-blue?style=for-the-badge&logo=python
  :target: https://arnall.frama.io/preciso/index.html
  
.. placeholder-for-doc-index


About
-----

**PreciSo** is a software written in `C++` and developped by `Michel Perez <https://michel.perez.net.free.fr>`_ and his co-workers.
It does classical nucleation and growth theories calculations for `precipitation <https://en.wikipedia.org/wiki/Classical_nucleation_theory>`_ in metallurgical systems. 

Getting Started
---------------

- Source code : `Gitlab Repository <https://framagit.org/arnall/preciso>`_
- To install **PreciSo**, check out the  `getting started <https://arnall.frama.io/preciso/quickstart.html#getting-started>`_ page.
- To try **PreciSo** with no installation, go here :

.. image:: https://mybinder.org/badge_logo.svg
 :target: https://mybinder.org/v2/gh/https%3A%2F%2Fframagit.org%2Farnall%2Fpreciso/19aa7ce7a2ecb19339c33bb7e8d499e7e23fb820?filepath=notebooks%2F01-Overview.ipynb


Authors
-------

* **Michel Perez** - `MichelPerezNet <http://michel.perez.net.free.fr/>`_
* **Thibaut Chaise**
* **Arnaud Allera** (Python API)
* And many others !


Contribution and support
------------------------

Contributions and feedback are welcome. The `issue tracker <https://framagit.org/arnall/preciso/issues>`_ is a great tool to submit questions, support requests and report bugs.


To cite this project
--------------------

Perez (M.), Dumont (M.) et Acevedo-Reyes (D.), **Implementation of the classical nucleation theory for precipitation**, Acta Mater., vol. 56, 2008, p. 2119–2132 `(PDF) <http://michel.perez.net.free.fr/Perez08.pdf>`_

License
-------

This project is licensed under the CECILL licence - see the `LICENSE.md <LICENSE.md>`_ file for details
